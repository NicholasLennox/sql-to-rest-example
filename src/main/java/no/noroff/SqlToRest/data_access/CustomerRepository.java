package no.noroff.SqlToRest.data_access;

import no.noroff.SqlToRest.models.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

public interface CustomerRepository {
    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(String custId);
    public Boolean addCustomer(Customer customer);
    public Boolean updateCustomer(Customer customer);
}
