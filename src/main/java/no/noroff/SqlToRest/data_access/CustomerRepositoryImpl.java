package no.noroff.SqlToRest.data_access;

import no.noroff.SqlToRest.logging.LogToConsole;
import no.noroff.SqlToRest.models.Customer;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/*
 This class serves as the encapsulation of all database interactions,
 it removes the implementation from the controllers to models - as they should only be responsible
 for handling user interactions and deciding what to do with it.
*/
@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final LogToConsole logger;

    // Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public CustomerRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    /*
     Need methods to serve the needs of the controller requests.
     Just mirror what your endpoints want.
    */

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Id,CompanyName, ContactName,Phone FROM customer");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("Id"),
                                resultSet.getString("CompanyName"),
                                resultSet.getString("ContactName"),
                                resultSet.getString("Phone")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customers;
    }

    public Customer getCustomerById(String custId){
        Customer customer = null;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Id,CompanyName, ContactName,Phone FROM customer WHERE Id = ?");
            preparedStatement.setString(1,custId);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                                resultSet.getString("Id"),
                                resultSet.getString("CompanyName"),
                                resultSet.getString("ContactName"),
                                resultSet.getString("Phone")
                        );
            }
            logger.log("Select specific customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return customer;
    }

    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(Id,CompanyName,ContactName,Phone) VALUES(?,?,?,?)");
            preparedStatement.setString(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getCompanyName());
            preparedStatement.setString(3,customer.getContactName());
            preparedStatement.setString(4,customer.getPhone());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Add customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }

    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customer SET Id = ?, CompanyName = ?, ContactName = ?, Phone = ? WHERE Id=?");
            preparedStatement.setString(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getCompanyName());
            preparedStatement.setString(3,customer.getContactName());
            preparedStatement.setString(4,customer.getPhone());
            preparedStatement.setString(5,customer.getCustomerId());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Update customer successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return success;
    }
}
